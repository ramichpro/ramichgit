<?php

namespace DBF\DBFormationBundle\Controller;

use DBF\DBFormationBundle\Entity\DBEtudiant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dbetudiant controller.
 *
 */
class DBEtudiantController extends Controller
{
    /**
     * Lists all dBEtudiant entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dBEtudiants = $em->getRepository('DBFormationBundle:DBEtudiant')->findAll();

        return $this->render('dbetudiant/index.html.twig', array(
            'dBEtudiants' => $dBEtudiants,
        ));
    }

    /**
     * Creates a new dBEtudiant entity.
     *
     */
    public function newAction(Request $request)
    {
        $dBEtudiant = new Dbetudiant();
        $form = $this->createForm('DBF\DBFormationBundle\Form\DBEtudiantType', $dBEtudiant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dBEtudiant);
            $em->flush();

            return $this->redirectToRoute('dbetudiant_show', array('id' => $dBEtudiant->getId()));
        }

        return $this->render('dbetudiant/new.html.twig', array(
            'dBEtudiant' => $dBEtudiant,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dBEtudiant entity.
     *
     */
    public function showAction(DBEtudiant $dBEtudiant)
    {
        $deleteForm = $this->createDeleteForm($dBEtudiant);

        return $this->render('dbetudiant/show.html.twig', array(
            'dBEtudiant' => $dBEtudiant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dBEtudiant entity.
     *
     */
    public function editAction(Request $request, DBEtudiant $dBEtudiant)
    {
        $deleteForm = $this->createDeleteForm($dBEtudiant);
        $editForm = $this->createForm('DBF\DBFormationBundle\Form\DBEtudiantType', $dBEtudiant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dbetudiant_edit', array('id' => $dBEtudiant->getId()));
        }

        return $this->render('dbetudiant/edit.html.twig', array(
            'dBEtudiant' => $dBEtudiant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dBEtudiant entity.
     *
     */
    public function deleteAction(Request $request, DBEtudiant $dBEtudiant)
    {
        $form = $this->createDeleteForm($dBEtudiant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dBEtudiant);
            $em->flush();
        }

        return $this->redirectToRoute('dbetudiant_index');
    }

    /**
     * Creates a form to delete a dBEtudiant entity.
     *
     * @param DBEtudiant $dBEtudiant The dBEtudiant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DBEtudiant $dBEtudiant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dbetudiant_delete', array('id' => $dBEtudiant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
