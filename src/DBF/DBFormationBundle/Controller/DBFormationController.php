<?php

namespace DBF\DBFormationBundle\Controller;

use DBF\DBFormationBundle\Entity\DBFormation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dbformation controller.
 *
 */
class DBFormationController extends Controller
{
    /**
     * Lists all dBFormation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dBFormations = $em->getRepository('DBFormationBundle:DBFormation')->findAll();

        return $this->render('dbformation/index.html.twig', array(
            'dBFormations' => $dBFormations,
        ));
    }

    /**
     * Creates a new dBFormation entity.
     *
     */
    public function newAction(Request $request)
    {
        $dBFormation = new Dbformation();
        $form = $this->createForm('DBF\DBFormationBundle\Form\DBFormationType', $dBFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dBFormation);
            $em->flush();

            return $this->redirectToRoute('dbformation_show', array('id' => $dBFormation->getId()));
        }

        return $this->render('dbformation/new.html.twig', array(
            'dBFormation' => $dBFormation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dBFormation entity.
     *
     */
    public function showAction(DBFormation $dBFormation)
    {
        $deleteForm = $this->createDeleteForm($dBFormation);

        return $this->render('dbformation/show.html.twig', array(
            'dBFormation' => $dBFormation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dBFormation entity.
     *
     */
    public function editAction(Request $request, DBFormation $dBFormation)
    {
        $deleteForm = $this->createDeleteForm($dBFormation);
        $editForm = $this->createForm('DBF\DBFormationBundle\Form\DBFormationType', $dBFormation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dbformation_edit', array('id' => $dBFormation->getId()));
        }

        return $this->render('dbformation/edit.html.twig', array(
            'dBFormation' => $dBFormation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dBFormation entity.
     *
     */
    public function deleteAction(Request $request, DBFormation $dBFormation)
    {
        $form = $this->createDeleteForm($dBFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dBFormation);
            $em->flush();
        }

        return $this->redirectToRoute('dbformation_index');
    }

    /**
     * Creates a form to delete a dBFormation entity.
     *
     * @param DBFormation $dBFormation The dBFormation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DBFormation $dBFormation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dbformation_delete', array('id' => $dBFormation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
