<?php

namespace DBF\DBFormationBundle\Controller;

use DBF\DBFormationBundle\Entity\DBSpecialite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dbspecialite controller.
 *
 */
class DBSpecialiteController extends Controller
{
    /**
     * Lists all dBSpecialite entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dBSpecialites = $em->getRepository('DBFormationBundle:DBSpecialite')->findAll();

        return $this->render('dbspecialite/index.html.twig', array(
            'dBSpecialites' => $dBSpecialites,
        ));
    }

    /**
     * Creates a new dBSpecialite entity.
     *
     */
    public function newAction(Request $request)
    {
        $dBSpecialite = new Dbspecialite();
        $form = $this->createForm('DBF\DBFormationBundle\Form\DBSpecialiteType', $dBSpecialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dBSpecialite);
            $em->flush();

            return $this->redirectToRoute('dbspecialite_show', array('id' => $dBSpecialite->getId()));
        }

        return $this->render('dbspecialite/new.html.twig', array(
            'dBSpecialite' => $dBSpecialite,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dBSpecialite entity.
     *
     */
    public function showAction(DBSpecialite $dBSpecialite)
    {
        $deleteForm = $this->createDeleteForm($dBSpecialite);

        return $this->render('dbspecialite/show.html.twig', array(
            'dBSpecialite' => $dBSpecialite,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dBSpecialite entity.
     *
     */
    public function editAction(Request $request, DBSpecialite $dBSpecialite)
    {
        $deleteForm = $this->createDeleteForm($dBSpecialite);
        $editForm = $this->createForm('DBF\DBFormationBundle\Form\DBSpecialiteType', $dBSpecialite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dbspecialite_edit', array('id' => $dBSpecialite->getId()));
        }

        return $this->render('dbspecialite/edit.html.twig', array(
            'dBSpecialite' => $dBSpecialite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dBSpecialite entity.
     *
     */
    public function deleteAction(Request $request, DBSpecialite $dBSpecialite)
    {
        $form = $this->createDeleteForm($dBSpecialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dBSpecialite);
            $em->flush();
        }

        return $this->redirectToRoute('dbspecialite_index');
    }

    /**
     * Creates a form to delete a dBSpecialite entity.
     *
     * @param DBSpecialite $dBSpecialite The dBSpecialite entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DBSpecialite $dBSpecialite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dbspecialite_delete', array('id' => $dBSpecialite->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
