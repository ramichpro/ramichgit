<?php

namespace DBF\DBFormationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DBFormationBundle:Default:index.html.twig');
    }
}
