<?php

namespace DBF\DBFormationBundle\Entity;

use DBF\DBFormationBundle\DBFormation;
use DBF\DBFormationBundle\DBSpecialite;
use Doctrine\ORM\Mapping as ORM;

/**
 * DBEtudiant
 *
 * @ORM\Table(name="d_b_etudiant")
 * @ORM\Entity(repositoryClass="DBF\DBFormationBundle\Repository\DBEtudiantRepository")
 */
class DBEtudiant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;



    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\ManyToOne(targetEntity="DBF\DBFormationBundle\Entity\DBSpecialite")
     */
    private $dbspecialite;

    /**
     * @ORM\ManyToMany(targetEntity="DBF\DBFormationBundle\Entity\DBFormation", cascade={"persist"})
     */
    private $dbformation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return DBEtudiant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return DBEtudiant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }





    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return DBEtudiant
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dbformation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dbspecialite
     *
     * @param DBSpecialite $dbspecialite
     *
     * @return DBEtudiant
     */
    public function setDbspecialite(DBSpecialite $dbspecialite)
    {
        $this->dbspecialite = $dbspecialite;

        return $this;
    }

    /**
     * Get dbspecialite
     *
     * @return DBSpecialite
     */
    public function getDbspecialite()
    {
        return $this->dbspecialite;
    }

    /**
     * Add dbformation
     *
     * @param DBFormation $dbformation
     *
     * @return DBEtudiant
     */
    public function addDbformation(DBFormation $dbformation)
    {
        $this->dbformation[] = $dbformation;

        return $this;
    }

    /**
     * Remove dbformation
     *
     * @param DBFormation $dbformation
     */
    public function removeDbformation(DBFormation $dbformation)
    {
        $this->dbformation->removeElement($dbformation);
    }

    /**
     * Get dbformation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDbformation()
    {
        return $this->dbformation;
    }
}
