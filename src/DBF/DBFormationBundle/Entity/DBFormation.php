<?php

namespace DBF\DBFormationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DBFormation
 *
 * @ORM\Table(name="d_b_formation")
 * @ORM\Entity(repositoryClass="DBF\DBFormationBundle\Repository\DBFormationRepository")
 */
class DBFormation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var int
     *
     * @ORM\Column(name="dure", type="integer")
     */
    private $dure;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer")
     */
    private $prix;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return DBFormation
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dure
     *
     * @param integer $dure
     *
     * @return DBFormation
     */
    public function setDure($dure)
    {
        $this->dure = $dure;

        return $this;
    }

    /**
     * Get dure
     *
     * @return int
     */
    public function getDure()
    {
        return $this->dure;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     *
     * @return DBFormation
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return int
     */
    public function getPrix()
    {
        return $this->prix;
    }
}
