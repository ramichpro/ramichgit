<?php

namespace RACH\RamichBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RamichBundle:Default:index.html.twig');
    }
}
