<?php

namespace RACH\RamichBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ParamController extends Controller
{
    public function paraAction(Request $request, $id)
    {
        $par=$request->get('para');
        return $this->render('ramich/parametre.html.twig',array('p'=>$par,'i'=>$id));
    }
}