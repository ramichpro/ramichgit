<?php

namespace RACH\RamichBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RamichController extends Controller
{
    public function raAction()
    {
        //$toto="c'est cool";
//        echo $toto;
        return $this->render('ramich/rapres.html.twig',
            array('tax'=>array('un'=>"premier",
                'deux'=>"deuxième",
                'trois'=>"troisième")));

            }
}
