<?php

namespace SYNF\FormsynfBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AutreController extends Controller
{
    public function indexAction()
    {
        return $this->render('form/rach.html.twig');
    }
}
